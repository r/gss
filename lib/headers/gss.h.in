/* gss.h --- Header file for GSSLib.                                  -*- c -*-
 * Copyright (C) 2003-2022 Simon Josefsson
 *
 * This file is part of the GNU Generic Security Service Library.
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of either:
 *
 *  * the GNU Lesser General Public License as published by the Free
 *    Software Foundation; either version 3 of the License, or (at
 *    your option) any later version.
 *
 * or
 *
 * * the GNU General Public License as published by the Free Software
 *   Foundation; either version 2 of the License, or (at your option)
 *   any later version.
 *
 * or both in parallel, as here.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received copies of the GNU General Public License
 * and the GNU Lesser General Public License along with this file.  If
 * not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _GSS_H
# define _GSS_H

# ifdef __cplusplus
extern "C"
{
# endif

  /**
   * GSS_VERSION
   *
   * Pre-processor symbol with a string that describe the header file
   * version number.  Used together with gss_check_version() to verify
   * header file and run-time library consistency.
   */
# define GSS_VERSION "@VERSION@"

  /**
   * GSS_VERSION_MAJOR
   *
   * Pre-processor symbol with a decimal value that describe the major
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 1.
   */
# define GSS_VERSION_MAJOR @MAJOR_VERSION@

  /**
   * GSS_VERSION_MINOR
   *
   * Pre-processor symbol with a decimal value that describe the minor
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 2.
   */
# define GSS_VERSION_MINOR @MINOR_VERSION@

  /**
   * GSS_VERSION_PATCH
   *
   * Pre-processor symbol with a decimal value that describe the patch
   * level of the header file version number.  For example, when the
   * header version is 1.2.3 this symbol will be 3.
   */
# define GSS_VERSION_PATCH @PATCH_VERSION@

  /**
   * GSS_VERSION_NUMBER
   *
   * Pre-processor symbol with a hexadecimal value describing the
   * header file version number.  For example, when the header version
   * is 1.2.3 this symbol will have the value 0x010203.
   */
# define GSS_VERSION_NUMBER @NUMBER_VERSION@

# include <gss/api.h>
# include <gss/ext.h>
  /* *INDENT-OFF* */
@INCLUDE_GSS_KRB5@
@INCLUDE_GSS_KRB5_EXT@
  /* *INDENT-ON* */

# ifdef __cplusplus
}
# endif

#endif				/* _GSS_H */
