# Bootstrap configuration.

# Copyright (C) 2006-2022 Free Software Foundation, Inc.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

gnulib_name=libgnu
source_base=src/gl
m4_base=src/gl/m4
local_gl_dir=src/gl
tests_base=src/gl/tests
gnulib_tool_option_extras=--without-tests
checkout_only_file=.gitignore

# gnulib modules used by this package.
gnulib_modules="
  autobuild
  base64
  error
  getline
  getopt-gnu
  git-version-gen
  havelib
  lib-msvc-compat
  lib-symbol-versions
  manywarnings
  pmccabe2html
  progname
  readme-release
  update-copyright
  valgrind-tests
  version-etc
"

# Additional xgettext options to use.  Use "\\\newline" to break lines.
XGETTEXT_OPTIONS=$XGETTEXT_OPTIONS'\\\
 --from-code=UTF-8\\\
 --flag=asprintf:2:c-format --flag=vasprintf:2:c-format\\\
 --flag=asnprintf:3:c-format --flag=vasnprintf:3:c-format\\\
 --flag=wrapf:1:c-format\\\
'

# If "AM_GNU_GETTEXT(external" or "AM_GNU_GETTEXT([external]"
# appears in configure.ac, exclude some unnecessary files.
# Without grep's -E option (not portable enough, pre-configure),
# the following test is ugly.  Also, this depends on the existence
# of configure.ac, not the obsolescent-named configure.in.  But if
# you're using this infrastructure, you should care about such things.

gettext_external=0
grep '^[	 ]*AM_GNU_GETTEXT(external\>' configure.ac > /dev/null &&
  gettext_external=1
grep '^[	 ]*AM_GNU_GETTEXT(\[external]' configure.ac > /dev/null &&
  gettext_external=1

if test $gettext_external = 1; then
  # Gettext supplies these files, but we don't need them since
  # we don't have an intl subdirectory.
  excluded_files='
      m4/glibc2.m4
      m4/intdiv0.m4
      m4/lcmessage.m4
      m4/lock.m4
      m4/printf-posix.m4
      m4/size_max.m4
      m4/uintmax_t.m4
      m4/ulonglong.m4
      m4/visibility.m4
      m4/xsize.m4
  '
fi

# Build prerequisites
buildreq="\
autoconf   2.59
automake   1.9.6
git        1.5.5
tar        -
"

bootstrap_post_import_hook ()
{
  ${GNULIB_SRCDIR}/gnulib-tool --import --lgpl=3orGPLv2 --lib=libgnu --source-base=lib/gl --local-dir=lib/gl --m4-base=lib/gl/m4 --doc-base=doc --aux-dir=build-aux --tests-base=lib/gl/tests --no-conditional-dependencies --libtool --macro-prefix=libgl --without-tests "gettext-h strverscmp"

  touch ChangeLog

  if ! gtkdocize; then
    echo "warning: gtkdocize missing -- gtk-doc manual will be missing"
    # rm because gtk-doc.make might be a link to a protected file
    rm -f gtk-doc.make
    echo "EXTRA_DIST =" > gtk-doc.make
    echo "CLEANFILES =" >> gtk-doc.make
  fi
}
